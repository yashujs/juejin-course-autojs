"ui";
engines.all().map((ScriptEngine) => {
    if (engines.myEngine().toString() !== ScriptEngine.toString()) {
        ScriptEngine.forceStop();
    }
});
ui.layout(
    <vertical>
        <button id="selectFile">选择文件</button>
    </vertical>
);
let imgRequestCode = 123;
ui.selectFile.click(function () {
    ui.selectFile.setEnabled(false);
    intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
    activity.startActivityForResult(intent, imgRequestCode);
    setTimeout(() => {
        ui.selectFile.setEnabled(true);
    }, 2000);
});

activity.getEventEmitter().on("activity_result", (requestCode, resultCode, data) => {
    if (requestCode == imgRequestCode) {
        if (resultCode == activity.RESULT_OK) {
            let uri = data.getData();
            log(uri);
            let bitmap = getBitmapFromUri(uri);
            log(bitmap);
        }
    }
});

function getBitmapFromUri(uri) {
    return android.provider.MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
}
