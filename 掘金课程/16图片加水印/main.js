engines.all().map((ScriptEngine) => {
    if (engines.myEngine().toString() !== ScriptEngine.toString()) {
        ScriptEngine.forceStop();
    }
});
let imgPath = files.path("./img.jpg");
let img = images.read(imgPath);
let newImg = img.clone();
var newBitmap = newImg.getBitmap();
var canvas = new Canvas(newBitmap);
var w = canvas.width,
    h = canvas.height;
var paint = new Paint();
paint.setStrokeWidth(5);
// paint.setColor(colors.RED);
paint["setColor(int)"](colors.RED);
paint.setStyle(Paint.Style.FILL);
paint.setAntiAlias(true);
let waterMark = "水印";

let rect = new android.graphics.Rect();
paint.getTextBounds(waterMark, 0, waterMark.length, rect);
let waterMarkW = rect.width();
let waterMarkH = rect.height();
log(waterMarkW, waterMarkH);
// 右下角加水印
canvas.drawText(waterMark, w - waterMarkW - 10, h - waterMarkH, paint);
let resultImg = canvas.toImage();
let resultImgPath = files.path("./result.jpg");
resultImg.saveTo(resultImgPath);
app.viewFile(resultImgPath);
canvas = null;
resultImg.recycle();
img.recycle();
newImg.recycle();
