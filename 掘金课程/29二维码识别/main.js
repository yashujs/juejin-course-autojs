engines.all().map((ScriptEngine) => {
    if (engines.myEngine().toString() !== ScriptEngine.toString()) {
        ScriptEngine.forceStop();
    }
});

runtime.loadDex("./zxing-core-3.4.1.dex");
importClass(com.google.zxing.BinaryBitmap);
importClass(com.google.zxing.common.HybridBinarizer);
importClass(com.google.zxing.RGBLuminanceSource);
importClass(com.google.zxing.MultiFormatReader);

/* -------------------------------------------------------------------------- */
let qrCodePath = files.path("./img.jpg");
let res = identifyLocalQrCode(qrCodePath);
log(res);
function identifyLocalQrCode(qrCodePath) {
    let img = images.read(qrCodePath);
    let bitmap = img.bitmap;
    let binaryBitmap;
    var w = bitmap.getWidth();
    var h = bitmap.getHeight();
    var pixels = util.java.array("int", w * h);
    bitmap.getPixels(pixels, 0, w, 0, 0, w, h);
    binaryBitmap = new BinaryBitmap(new HybridBinarizer(new RGBLuminanceSource(w, h, pixels)));
    var qrCodeResult = new MultiFormatReader().decode(binaryBitmap);
    var result = qrCodeResult.getText().toString();
    img.recycle();
    return result;
}
