"ui";
ui.layout(
    <vertical>
        <canvas id="canvas"></canvas>
    </vertical>
);
let paint = new Paint();
paint["setColor(int)"](colors.parseColor("#4fdfff"));
paint.setStyle(Paint.Style.STROKE);
paint.setStrokeWidth(10);
ui.canvas.on("draw", function (canvas) {
    canvas.drawColor(colors.RED);
    canvas.drawRect(110, 110, 300, 300, paint);
});
