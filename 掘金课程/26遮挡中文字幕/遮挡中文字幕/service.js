function getOrientation() {
    return context.getResources().getConfiguration().orientation;
}

function saveWindowInfo(window, storage) {
    let windowX = window.getX();
    let windowY = window.getY();
    let windowWidth = window.getWidth();
    let windowHeight = window.getHeight();
    let windowData = {
        orientation: context.getResources().getConfiguration().orientation,
        x: windowX,
        y: windowY,
        width: windowWidth,
        height: windowHeight,
    };
    log("存储窗口信息");
    log(windowData);
    if (windowData.orientation == 1) {
        storage.put("竖屏", windowData);
    } else {
        storage.put("横屏", windowData);
    }
}

function changeWindowSizeBottom(distance, windowRawInfo) {
    let window = windowRawInfo.window;
    let windowX = windowRawInfo.rawX;
    let windowY = windowRawInfo.rawY;
    let windowWidth = windowRawInfo.rawWidth;
    let windowHeight = windowRawInfo.rawHeight;
    windowHeight = windowHeight + distance;
    ui.run(function () {
        window.setPosition(parseInt(windowX), parseInt(windowY));
        window.setSize(parseInt(windowWidth), parseInt(windowHeight));
    });
}

function changeWindowSizeTop(distance, windowRawInfo) {
    let window = windowRawInfo.window;
    let windowX = windowRawInfo.rawX;
    let windowY = windowRawInfo.rawY;
    let windowWidth = windowRawInfo.rawWidth;
    let windowHeight = windowRawInfo.rawHeight;
    windowY = windowY + distance;
    windowHeight = windowHeight - distance;
    ui.run(function () {
        window.setPosition(parseInt(windowX), parseInt(windowY));
        window.setSize(parseInt(windowWidth), parseInt(windowHeight));
    });
}
function changeWindowSizeRight(distance, windowRawInfo) {
    let window = windowRawInfo.window;
    let windowX = windowRawInfo.rawX;
    let windowY = windowRawInfo.rawY;
    let windowWidth = windowRawInfo.rawWidth;
    let windowHeight = windowRawInfo.rawHeight;
    windowWidth = windowWidth + distance;
    ui.run(function () {
        window.setPosition(parseInt(windowX), parseInt(windowY));
        window.setSize(parseInt(windowWidth), parseInt(windowHeight));
    });
}

function changeWindowSizeLeft(distance, windowRawInfo) {
    let window = windowRawInfo.window;
    let windowX = windowRawInfo.rawX;
    let windowY = windowRawInfo.rawY;
    let windowWidth = windowRawInfo.rawWidth;
    let windowHeight = windowRawInfo.rawHeight;
    windowX = windowX + distance;
    windowWidth = windowWidth - distance;
    ui.run(function () {
        window.setPosition(parseInt(windowX), parseInt(windowY));
        window.setSize(parseInt(windowWidth), parseInt(windowHeight));
    });
}
function inWhichArea(rx, ry, window) {
    let windowWidth = window.getWidth();
    let windowHeight = window.getHeight();
    let ratio = 1 / 5;
    let edgeAreaWidth = windowWidth * ratio;
    let edgeAreaHeight = windowHeight * ratio;
    if (rx < edgeAreaWidth) {
        return "left";
    } else if (rx > windowWidth - edgeAreaWidth) {
        return "right";
    } else if (ry < edgeAreaHeight) {
        return "top";
    } else if (ry > windowHeight - edgeAreaHeight) {
        return "bottom";
    } else {
        return "center";
    }
}
function enableScreenListener() {
    let filter = new android.content.IntentFilter();
    filter.addAction("android.intent.action.CONFIGURATION_CHANGED");
    let br = new android.content.BroadcastReceiver({
        onReceive: function (context, intent) {
            if (intent.getAction() == "android.intent.action.CONFIGURATION_CHANGED") {
                events.broadcast.emit("屏幕方向发生改变", "");
            }
        },
    });
    context.registerReceiver(br, filter);
    events.on("exit", () => {
        context.unregisterReceiver(br);
    });
}
module.exports = {
    getOrientation: getOrientation,
    saveWindowInfo: saveWindowInfo,
    changeWindowSizeBottom: changeWindowSizeBottom,
    changeWindowSizeTop: changeWindowSizeTop,
    changeWindowSizeRight: changeWindowSizeRight,
    changeWindowSizeLeft: changeWindowSizeLeft,
    inWhichArea: inWhichArea,
    enableScreenListener: enableScreenListener,
};
