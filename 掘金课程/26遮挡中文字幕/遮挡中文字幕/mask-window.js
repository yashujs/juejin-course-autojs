let service = require("./service.js");
let config = require("./config.js");
let windowWidth = config.windowWidth;
let windowHeight = config.windowHeight;
let dw = config.dw;
let dh = config.dh;
let storage = config.storage;
// TODO: 临时测试
// storage.clear();

let { enableScreenListener, getOrientation, saveWindowInfo, inWhichArea, changeWindowSizeLeft, changeWindowSizeRight, changeWindowSizeTop, changeWindowSizeBottom } = service;
enableScreenListener();

function MaskWindow() {
    let window = floaty.rawWindow(<View id="maskView" bg="#ff0000" w="*" h="*"></View>);
    this.window = window;
    window.setSize(windowWidth, windowHeight);
    events.on("exit", function () {
        window && window.close();
    });
    setInterval(() => {}, 1000);
    window.setPosition(dw / 2 - windowWidth / 2, dh / 2 - windowHeight / 2);

    //记录按键被按下时的触摸坐标
    var x = 0,
        y = 0;
    //记录按键被按下时的悬浮窗位置
    var windowX, windowY;
    //记录按键被按下时的触摸坐标相对于悬浮窗控件的坐标
    var rx, ry;
    let inEdgeArea;
    let rawWidth, rawHeight;
    let windowRawInfo;
    let areaType;
    window.maskView.setOnTouchListener(function (view, event) {
        switch (event.getAction()) {
            case event.ACTION_DOWN:
                x = event.getRawX();
                y = event.getRawY();
                windowX = window.getX();
                windowY = window.getY();
                rx = event.getX();
                ry = event.getY();
                rawWidth = window.getWidth();
                rawHeight = window.getHeight();
                windowRawInfo = {
                    rawX: windowX,
                    rawY: windowY,
                    rawWidth: rawWidth,
                    rawHeight: rawHeight,
                    window: window,
                };
                areaType = inWhichArea(rx, ry, window);
                log("areaType = " + areaType);
                return true;
            case event.ACTION_MOVE:
                switch (areaType) {
                    case "left":
                        changeWindowSizeLeft(event.getRawX() - x, windowRawInfo);
                        break;
                    case "right":
                        changeWindowSizeRight(event.getRawX() - x, windowRawInfo);
                        break;
                    case "top":
                        changeWindowSizeTop(event.getRawY() - y, windowRawInfo);
                        break;
                    case "bottom":
                        changeWindowSizeBottom(event.getRawY() - y, windowRawInfo);
                        break;
                    case "center":
                        window.setPosition(windowX + (event.getRawX() - x), windowY + (event.getRawY() - y));
                        break;
                }
                return true;
            case event.ACTION_UP:
                saveWindowInfo(window, storage);
                return true;
        }
        return true;
    });
    let that = this;
    events.broadcast.on("屏幕方向发生改变", function () {
        log("屏幕方向发生改变");
        that.updateWindow();
    });

    //启用按键监听
    events.observeKey();
    //监听音量下键弹起
    events.onKeyDown("volume_down", function (event) {
        log("音量下键被按下");
        // 停止自己
        window.close();
        engines.myEngine().forceStop();
    });
    //保持脚本运行
    setInterval(() => {}, 1000);
}
MaskWindow.prototype.clearData = function () {
    storage.clear();
};
MaskWindow.prototype.updateWindow = function () {
    let currentOrientation = getOrientation();
    if (currentOrientation === 1) {
        log("当前是竖屏");
        let windowData = storage.get("竖屏");
        if (!windowData) {
            return;
        }
        log("有竖屏数据");
        log(windowData);
        let { x, y, width, height } = windowData;
        width = width > dw ? dw : width;
        height = height > dh ? dh : height;
        this.window.setSize(width, height);
        this.window.setPosition(x, y);
    } else {
        log("当前是横屏");
        let windowData = storage.get("横屏");
        if (!windowData) {
            this.window.setSize(windowWidth, windowHeight);
            this.window.setPosition(dh / 2 - windowWidth / 2, dw / 2 - windowHeight / 2);
            return;
        }
        let { x, y, width, height } = windowData;
        width = width > dh ? dh : width;
        height = height > dw ? dw : height;
        this.window.setSize(width, height);
        this.window.setPosition(x, y);
    }
};

module.exports = MaskWindow;
